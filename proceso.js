let arrayProductos = [];

let producto = {
    descripcion: "",
    imagen: ""
}

if (localStorage.getItem('productos')) {
    arrayProductos = JSON.parse(localStorage.productos);
} else {
    producto = {
        descripcion: " Fire TV Stick 4K Ultra HD con mando por voz Alexa",
        imagen: "https://revistabyte.es/wp-content/uploads/2020/05/1-323x360.jpg.webp"
    }

    arrayProductos.push(producto);

    producto = {
        descripcion: ' TV Samsung 4K UHD de 50”',
        imagen: "https://revistabyte.es/wp-content/uploads/2020/05/4-425x360.jpg.webp"
    }
    arrayProductos.push(producto);
}
document.querySelector("#producto").innerHTML = armaTemplate();


function agregar() {
    let des = document.querySelector("#desc").value.trim();
    let img = document.querySelector("#dirImg").value.trim();
    if(des.length === 0 || img.length === 0 )return;

    producto = {
        descripcion: des,
        imagen: img
    }

    arrayProductos.push(producto);
    console.log(arrayProductos);

    document.querySelector("#producto").innerHTML = armaTemplate();
}

function armaTemplate() {
    let template = '';
    for (let i = 0; i < arrayProductos.length; i++) {
        producto = arrayProductos[i];
        template += `<article>
                        <div class ="trash"  onclick="eliminarItem(${i})"><img src="trash-can.png"></div>
                        <h3 class ="descripcion">${producto.descripcion}</h3>
                        <img src="${producto.imagen}" class ="imagen">
                    </article>`

    }
    return template;
}

function listado() {
    if (arrayProductos.length > 0 ) localStorage.setItem("productos",JSON.stringify(arrayProductos));
    location.href = 'otrapagina.html';
}

function eliminarItem(nroProd) {
    console.log("Hive click en el producto:",nroProd);
    arrayProductos.splice(nroProd,1);
    if(arrayProductos.length === 0)localStorage.removeItem('productos');
    document.querySelector("#producto").innerHTML = armaTemplate();

}